package com.micovery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RVExtensionDemo {

  private static RVExtensionDemo _instance = new RVExtensionDemo();

  private RVExtensionDemo() {
  }

  public String date(String format) {
    DateFormat dateFormat = new SimpleDateFormat(format);
    return dateFormat.format(new Date());
  }

  public static String run(String entry) {
    try {
      return MethodInvoke.run(_instance, entry);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }
}