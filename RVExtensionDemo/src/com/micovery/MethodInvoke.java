package com.micovery;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.*;

@XmlRootElement(name = "MI")
public class MethodInvoke {
  @XmlElement(name = "M")
  public String method = null;

  @XmlElementWrapper(name = "AL")
  @XmlElement(name = "A")
  public ArrayList<String> arguments = null;

  public static String run(Object instance, String data) {
    try {
      MethodInvoke invoke = null;
      try {
        JAXBContext jaxbContext = JAXBContext.newInstance(MethodInvoke.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        invoke = (MethodInvoke) jaxbUnmarshaller.unmarshal(new StringReader(data));
      }
      catch (JAXBException e) {
      }

      if (invoke == null)
        return "";

      Method myMethod = null;
      try {
        Class[] argumentTypes = new Class[invoke.arguments.size()];
        for (int i = 0; i < argumentTypes.length; i++)
          argumentTypes[i] = String.class;
        myMethod = instance.getClass().getMethod(invoke.method, argumentTypes);
      }
      catch (NoSuchMethodException ex) {
      }

      if (myMethod == null)
        return "";

      String[] arguments = invoke.arguments.toArray(new String[0]);
      String result = (String) myMethod.invoke(instance, (Object[]) arguments);

      if (result == null)
        return "";

      return result;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return "";
  }
}